
<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 * Template Name: Speciale Soggiorno
 * Description: Speciale Soggiorno
 */

$templates = array('pages/services-speciale-soggiorno.twig');
$context = Timber::get_context();
$context['post'] = new TimberPost();

$context['products'] = Timber::get_posts(array(
    "post_type" => "speciale_soggiorno",
    "posts_per_page" => -1,
    'numberposts'  => -1,
));

Timber::render( $templates, $context );