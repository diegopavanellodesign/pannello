import $ from "jquery";
import "slick-carousel";

export default class Carousels {
	constructor(){
		this.carousel = {
			background: document.querySelector('.slider-background'),
			reference: document.querySelector('.container-reference'),
			twoSlider: document.querySelector('.slider-two-column'),
		};
	}

	init() {
		if(this.carousel.background) { this.sliderBackground(); }
		if(this.carousel.reference) { this.referenceSlider(); }
		if(this.carousel.twoSlider) { this.twoColumnSlider(); }
	}
    
	sliderBackground = () => {
      $('.slider-background').not('.slick-initialized').slick({
          infinite: true,
          autoplay: true,
          autoplaySpeed: 3000,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots:false,
      });
	}

  referenceSlider = () => {
    console.log('Prova')
    $('.container-reference').not('.slick-initialized').slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: false,
      dots:true,
      responsive: [
          {
            breakpoint:769,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              dots: true,
              centerMode: false,
            }
          }
      ]
  });
  }
  twoColumnSlider = () => {
    $('.slider-two-column').not('.slick-initialized').slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots:true,
  });
  }
}
