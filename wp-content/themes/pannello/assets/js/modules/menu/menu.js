export default class Menu {
    constructor() {
        this.menu = {
            toggler: document.getElementById('menu-toggler'),
            main: document.getElementById('main-menu'),
            dropdown : document.querySelector('header .dropdown'),
        }

        this.lang = {
            toggler: document.querySelector('#select-language .dropdown-toggler'),
            menu: document.querySelector('#select-language .dropdown-menu'),
        }
    }

    init() {
        this.toggle();
        // if(!Sniff.features.mobile){ this.language(); }
    }

    toggle = () => {
        this.menu.toggler.addEventListener('click', () => {
            this.menu.main.classList.toggle('active');
            this.menu.toggler.classList.toggle('active');
            this.lang.menu.classList.remove('show');
        });
        this.menu.dropdown.addEventListener('click', () => {
            this.menu.dropdown.classList.toggle('active-dropdown');
        });
    }
   
    // language = () => {
    //     this.lang.toggler.addEventListener('click', () => {
    //         this.lang.menu.classList.toggle('show');
    //     });
    // }
}
$( "#menu-toggler" ).click(function() {
    $('body').toggleClass('overflow-body');
  });