import $ from "jquery";
import gsap from "gsap";
import imagesLoaded from 'imagesloaded';
import ScrollTrigger from 'gsap/ScrollTrigger';
import LocomotiveScroll from 'locomotive-scroll';

gsap.registerPlugin(ScrollTrigger);

export default class Scroll {
    constructor() {
        this.scroll = document.querySelector('.app-scroll');

        this.header = document.querySelector('header');
        this.footer = document.querySelector('footer');

        this.virtualscroll = new LocomotiveScroll({
            el: document.querySelector('.app-scroll'),
            smooth: true
        });
    }

    init() {
        if(Sniff.features.mobile === false){
            this.render();
            this.manipulate();
        } else if (Sniff.features.mobile === true) {
            this.anchors();
        }
    }

    render = () => {
        if (app.loading === true) return;
        console.log('Render - scroll.js');
        // this.anchors();

        const locoScroll = this.virtualscroll;
        // each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
        locoScroll.on("scroll", ScrollTrigger.update);

        // tell ScrollTrigger to use these proxy methods for the ".smooth-scroll" element since Locomotive Scroll is hijacking things
        ScrollTrigger.scrollerProxy(this.scroll, {
          scrollTop(value) {
            return arguments.length ? locoScroll.scrollTo(value, 0, 0) : locoScroll.scroll.instance.scroll.y;
          }, // we don't have to define a scrollLeft because we're only scrolling vertically.
          getBoundingClientRect() {
            return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
          },
          // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
          pinType: this.scroll.style.transform ? "transform" : "fixed"
        });

        ScrollTrigger.defaults({
            scroller: this.scroll,
        })

        ScrollTrigger.addEventListener('refresh', () => this.virtualscroll.update() );
        ScrollTrigger.refresh();

        // this.sync();
    }

    anchors = () => {
        console.log('Anchors - scroll.js')
        // Select all links with hashes
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .on('click', function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top - $('header').height() - 50
                        }, 1000);
                    }
                }
            }
        );
    }

    sync = () => {
        // ScrollTrigger.addEventListener("scrollStart", () => {
        //     console.log(ScrollTrigger.getAll())
        // });
        // Each time window updates, should refresh ScrollTrigger and then update LocomotiveScroll
        ScrollTrigger.addEventListener('refresh', () => this.virtualscroll.update() );
        ScrollTrigger.refresh();
    }

    resize = () => {
        this.bounding = this.section.getBoundingClientRect();
    }

    destroy = () => {
        console.log('Destroing Scroll')
        this.virtualscroll.destroy();
    }

    update = () => {
        this.virtualscroll.update();
    }

    manipulate = () => {
        // this.scroll.prepend(this.header);
        this.scroll.appendChild(this.footer);
    }

    progressiveimgs = () => {
        console.log('Progressive Images Active!');

        document.querySelectorAll('[data-src]').forEach((image) => {
            ScrollTrigger.create({
                trigger: image,
                start: '-=50% bottom',
                end: '-=10% bottom',
                toggleClass: 'progressive',
                markers: true,
                once: true,
            });
        });

        document.querySelectorAll('[data-background]').forEach((background) => {
            ScrollTrigger.create({
                trigger: background,
                start: '-=50% bottom',
                end: '-=10% bottom',
                toggleClass: 'progressive',
                markers: true,
                once: true,
            });
        });

        this.virtualscroll.on('scroll',() => {
            document.querySelectorAll('[data-src]').forEach((image) => {
                if(image.classList.contains('progressive')){
                    var fullsize = image.getAttribute('data-src');
                    console.log(fullsize);
                    image.setAttribute('src', fullsize);
                    image.classList.remove('progressive');
                }
            });

            document.querySelectorAll('[data-background]').forEach((background) => {
                if(background.classList.contains('progressive')){
                    var fullsize = background.getAttribute('src');
                    background.setAttribute('src', fullsize);
                    background.classList.remove('progressive');
                }
            });
        });
    }
}
