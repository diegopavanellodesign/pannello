export default class Cookies {
	run() {
		this.cookieConsent();
	}

	cookieConsent() {
		window.start.init({
            Palette:"palette1",
            Mode:"banner bottom",
            Theme:"edgeless",
            Message:"This site involves the use of third-party cookies. By closing this banner, scrolling this page, or continuing to browse without changing the cookie settings, their use is considered accepted.",
            ButtonText:"<i class='icon-close'></i>",
            LinkText:"More information",
            Location:"/#",
            Time:"5",})
	}
}
