import Sniffer from 'snifferjs';

// store
import { app } from '../utils/store';

import Scroll from '../modules/scroll/scroll';
import Header from '../modules/header/header';
import Carousels from '../modules/carousel/carousel';
// import Video from '../modules/video/video';
// import Gallery from '../modules/gallery/gallery';
import Footer from '../modules/footer/footer';

export default class Components {
    constructor() {
        this.header = document.querySelector('header');
        this.menu = document.getElementById('main-menu');
        this.footer = document.querySelector('footer');
        this.video = document.querySelector('video');
        this.gallery = document.getElementById('gallery');
        this.carousel = document.querySelector('.carousel');

        this.page = {
            test: document.querySelector('.page-test'),
        }

        this.post = {
            test: document.querySelector('.page-test-article'),
        }
    }

    init(){
        // app.scroll = new Scroll();
        // app.scroll.init();

        // Progressive Loading Image (Need 'data-src' or 'data-background' attribute in your HTML)
        // app.scroll.progressiveimgs();

        if(this.header){
            this.header = new Header();
            this.header.init();
        }

        if(this.footer){
            this.footer = new Footer();
            this.footer.init();
        }

        if(this.carousel){
            console.log('Ciao');
            this.carousel = new Carousels();
            this.carousel.init();
        }

        // if(this.video){
        //     this.video = new Video();
        //     this.video.init();
        // }

        // if(this.gallery){
        //     this.gallery = new Gallery();
        //     this.gallery.init();
        // }
    }

    destroy = () => {
        console.log('Destroy Components')
        // Destroy Components using JS
    }
}
