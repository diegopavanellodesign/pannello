// Renderers
import Renderer from './renderers/renderer';

// UI Components/Modules
// import Preload from './modules/preload/preload';
import Menu from './modules/menu/menu';

// User Detect
import Detect from './utils/detect';

// App
import { app } from './utils/store';

// Init
app.detect = new Detect();
app.detect.init();

app.nav = new Menu();
app.nav.init();

// app.loader = new Preload();
// app.loader.init();

app.renderer = new Renderer();
app.renderer.init();
