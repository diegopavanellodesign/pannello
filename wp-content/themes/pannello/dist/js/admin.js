window.addEventListener('DOMContentLoaded', function () {

    jQuery('[data-wp-lists="list:user"] tr').each(function(){
        var role = jQuery(this).find('.role').text().toLowerCase().replace(/ /g, '_');
        jQuery(this).attr('role', role);
        jQuery(this).find('.row-actions .view').remove();
    });
    jQuery('[data-wp-lists="list:user"] [role]').not('body.administrator [data-wp-lists="list:user"] [role]').each(function(){
        // [role="client"],[role="staff"],[role="site_manager"]
        if(jQuery(this).attr('role') != 'client' && jQuery(this).attr('role') != 'staff' && jQuery(this).attr('role') != 'site_manager' && jQuery(this).attr('role') != 'user_manager'){
            jQuery(this).remove();
        }
    });

    //SITE MANAGER
    jQuery('.site_manager select option[value="administrator"]').remove();
    jQuery('.site_manager select option[value="subscriber"]').remove();
    jQuery('.site_manager select option[value="contributor"]').remove();
    jQuery('.site_manager select option[value="author"]').remove();
    jQuery('.site_manager select option[value="editor"]').remove();

    // USER MANAGER
    jQuery('.user_manager select option[value="administrator"]').remove();
    jQuery('.user_manager select option[value="subscriber"]').remove();
    jQuery('.user_manager select option[value="contributor"]').remove();
    jQuery('.user_manager select option[value="author"]').remove();
    jQuery('.user_manager select option[value="editor"]').remove();
    jQuery('.user_manager select option[value="site_manager"]').remove();
    jQuery('.user_manager select option[value="staff"]').remove();

    //STAFF
    jQuery('.staff select option[value="administrator"]').remove();
    jQuery('.staff select option[value="subscriber"]').remove();
    jQuery('.staff select option[value="contributor"]').remove();
    jQuery('.staff select option[value="author"]').remove();
    jQuery('.staff select option[value="editor"]').remove();
    jQuery('.staff select option[value="site_manager"]').remove();


    jQuery('.site_manager #menu-appearance').remove();
    jQuery('.site_manager #wpbody .update-nag').remove();


    jQuery('.post-type-file .acf-postbox.seamless .postbox-header').remove();
    jQuery('.post-type-file #kinddiv').remove();
    jQuery('#postexcerpt').addClass('acf-postbox seamless');
    jQuery('#postexcerpt div:last-child').removeClass('inside');
    jQuery('#postexcerpt div:last-child label').removeClass('screen-reader-text');
    jQuery('#postexcerpt div:last-child p').remove();


    // Checkbox and SubCheckbox UI
    jQuery('.post-type-file .kind-selection .acf-checkbox-list li, .post-type-registration-request [data-name="content_permission"] .acf-checkbox-list li, #profile-page [data-name="content_permission"] .acf-checkbox-list li').each(function(){
        if(jQuery(this).find('ul').length > 0){
            console.log(jQuery(this).find('ul').length);
            console.log(jQuery(this).find('input').is(":checked"));
            if(jQuery(this).find('input').is(":checked")){
                jQuery(this).find('ul').removeClass('hidden');
            } else {
                jQuery(this).find('ul').addClass('hidden');
            }
        }

        jQuery(this).find('input').on('click', function(){
            if(jQuery(this).parent().parent().find('ul').length > 0){
                if(jQuery(this).is(":checked")){
                    jQuery(this).parent().parent().find('ul').removeClass('hidden');
                } else {
                    jQuery(this).parent().parent().find('ul').addClass('hidden');
                    jQuery(this).parent().parent().find('ul li').each(function(){
                        jQuery(this).find('input').prop('checked', false);
                    });
                }
            }
        });
    });
});
