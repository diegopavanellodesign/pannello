<?php
/**
* Based on Timber starter-theme
* https://github.com/timber/starter-theme
* Developed © Gioele Chiappani
*
* @package  WordPress
* @subpackage  Timber
* @since   Timber 0.1
*/

/**
* If you are installing Timber as a Composer dependency in your theme, you'll need this block
* to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
* plug-in, you can safely delete this block.
*/
$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists( $composer_autoload ) ) {
	require_once $composer_autoload;
	$timber = new Timber\Timber();
}

/**
* This ensures that Timber is loaded and available as a PHP class.
* If not, it gives an error message to help direct developers on where to activate
*/
if ( ! class_exists( 'Timber' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>'.__('Timber non attivato. Assicurati di aver attivato il plugin:','timber').'<a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
			return get_stylesheet_directory() . '/static/no-timber.html';
		}
	);
	return;
}

/**
* Sets the directories (inside your theme) to find .twig files
*/
Timber::$dirname = array( 'templates', 'views' );

/**
* By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
* No prob! Just set this value to true
*/
Timber::$autoescape = false;


/**
* We're going to configure our theme inside of a subclass of Timber\Site
* You can move this to its own file and include here via php's include("MySite.php")
*/
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'admin_head', array( $this, 'admin_styles') );
		add_action( 'admin_menu', array( $this, 'admin_pages' ) );
		add_filter( 'admin_body_class', array( $this, 'current_user_role' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts') );
		add_action( 'init', array( $this, 'add_site_manager_role') );
		add_action( 'init', array( $this, 'add_option_page' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );
		add_filter( 'use_block_editor_for_post_type', '__return_false', 10 );
		parent::__construct();
	}
	/** This is where you can add your functions to Admin Backend <head> */
	public function admin_styles() { }
	/** This is where you can add your assets (CSS&JS) for your Admin Backend */
    public function admin_scripts() {
		wp_enqueue_style('admin', get_stylesheet_directory_uri() . '/dist/css/admin.min.css');
        wp_enqueue_script('admin', get_template_directory_uri() . "/dist/js/admin.js");
	}
	/** This is where you can add/remove Admin pages/menus */
    public function admin_pages(){
		if (current_user_can('site_manager')) {
			remove_menu_page( 'edit.php' );
			remove_menu_page( 'edit-comments.php' );
            remove_menu_page( 'tools.php' );
            remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
            remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu
            remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' ); // hide the customizer submenu
            remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftools.php&#038;autofocus%5Bcontrol%5D=background_image' ); // hide the background submenu

            add_menu_page(
                'Menu', 'Menu', 'edit_posts', '/nav-menus.php', '', 'dashicons-menu', 4
            );
        }
	}
    
	/** Creating ACF Options Page 'Informazioni' */
	public function add_option_page() {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title'  => 'Informazioni Generali',
				'menu_title'  => 'Informazioni',
				'menu_slug'   => 'your-project-settings',
				'capability'  => 'edit_posts',
				'redirect'    => false,
				'icon_url'    => 'dashicons-index-card',
			));
		}
	}
	/** This is where you can register custom post types. */
	public function register_post_types() { }
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() { }
	/** This is where you can add your assets */
	public function enqueue_assets() {
		wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');
		wp_enqueue_style('bootstrap-icon', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css');
		wp_enqueue_style('locomotive', 'https://cdn.jsdelivr.net/npm/locomotive-scroll@4.1.0/dist/locomotive-scroll.min.css');
		wp_enqueue_style('slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
		wp_enqueue_style('slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css');
		wp_enqueue_style('lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.10.0/css/lightgallery.min.css');

		// Custom CSS
		wp_enqueue_style('style', get_stylesheet_directory_uri() . '/dist/css/main.min.css');

		// Update WP jQuery Version
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', '', '', true);
		wp_enqueue_script('jquery');

		// Inject Scripts
        

		// Custom JS
		wp_enqueue_script('vendors', get_template_directory_uri() . "/dist/js/vendors~index.min.js",'','',true);
        wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', '', '', true );
		wp_enqueue_script('scrolltrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.0/ScrollTrigger.min.js', '', '', true);
		wp_enqueue_script('main', get_template_directory_uri() . "/dist/js/index.min.js",'','',true);
        
	}

	/** Add User Role Class to Body **/
    public function current_user_role($classes) {
        global $current_user;
        $user_role = array_shift($current_user->roles);
        /* Adds the user id to the admin body class array */
        $classes = $user_role;
        return $classes;
    }

	public function add_site_manager_role() {
        add_role(
            'site_manager',
            __('Site Manager', 'your-project'),
            array(
                'manage_categories' => true,
                'manage_links' => true,
                'upload_files' => true,
                'unfiltered_html' => true,
                'edit_posts' => true,
                'edit_others_posts' => true,
                'edit_published_posts' => true,
                'publish_posts' => true,
                'edit_pages' => true,
                'read' => true,
                'level_7' => true,
                'level_6' => true,
                'level_5' => true,
                'level_4' => true,
                'level_3' => true,
                'level_2' => true,
                'level_1' => true,
                'level_0' => true,
                'edit_others_pages' => true,
                'edit_published_pages' => true,
                'publish_pages' => true,
                'delete_pages' => true,
                'delete_others_pages' => true,
                'delete_published_pages' => true,
                'delete_posts' => true,
                'delete_others_posts' => true,
                'delete_published_posts' => true,
                'delete_private_posts' => true,
                'edit_private_posts' => true,
                'read_private_posts' => true,
                'delete_private_pages' => true,
                'edit_private_pages' => true,
                'read_private_pages' => true,
                'copy_posts' => true,
                'edit_theme_options' => true,
                'edit_attachments' => true,
                'delete_attachments' => true,
                'read_others_attachments' => true,
                'edit_others_attachments' => true,
                'delete_others_attachments' => true,
                'delete_users' => true,
                'create_users' => true,
                'list_users' => true,
                'remove_users' => true,
                'promote_users' => true,
                'promote_users_to_higher_level' => true,
            )
        );
    }

	/** This is where you add some context
	*
	* @param string $context context['this'] Being the Twig's {{ this }}.
	*/
	public function add_to_context( $context ) {
		$context['foo']   = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::context();';
		// Get Fields from Options
		$context['options'] = get_fields('option');
		$context['holder'] = get_field('holder','option');
		// Menus
		$context['menu']  = new Timber\Menu();
		$context['main_menu']  = new Timber\Menu('main_menu');
		$context['language_switcher'] = new Timber\Menu('language_switcher');

		$context['site']  = $this;

		$context['url_parameters'] = Timber\URLHelper::get_params();
		$context['cookie_test'] = isset($_COOKIE['test']) ? $_COOKIE['bar'] : 'foo';
		return $context;
	}

	public function custom_cookies(){
		setcookie("test", 'foo', strtotime('+15 days'), '/', 'your-project.noooserver.com');
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a hard-coded <title> tag in the document head, and expect WordPress to provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );

		/*
		* Switch default core markup for search form, comment form, and comments to output valid HTML5.
		*/
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		* Enable support for Post Formats.
		*
		* See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This Would return 'foo bar!'.
	*
	* @param string $text being 'foo', then returned 'foo bar!'.
	*/
	public function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	/** This is where you can add your own functions to twig.
	*
	* @param string $twig get extension.
	*/
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFilter( new Twig\TwigFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		$twig->addFilter( new Timber\Twig_Filter( 'slugify', function( $title ) {
	        return sanitize_title( $title );
	    } ) );
		
		return $twig;
	}
    

}

new StarterSite();
