<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 * Template Name: Consulenza
 * Description: Consulenza
 */

$templates = array('pages/services-consulenza.twig');
$context = Timber::get_context();
$context['post'] = new TimberPost();

$context['products'] = Timber::get_posts(array(
    "post_type" => "servizio_consulenza",
    "posts_per_page" => -1,
    'numberposts'  => -1,
));

Timber::render( $templates, $context );